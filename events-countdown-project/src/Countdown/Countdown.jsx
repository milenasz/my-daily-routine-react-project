import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { hourMinuteToSeconds, secondsToHourMinuteSecond } from '../utils';

import Overlay from './Overlay/Overlay';
import './Countdown.css';
import './../../node_modules/semantic-ui-css/semantic.css';

class Countdown extends Component {

    constructor(props) {
        super(props);
        this.props = props;
    }

    render() {
        const eventInSecound = hourMinuteToSeconds(this.props.hour, this.props.minute);
        const nowInSeconds = hourMinuteToSeconds(this.props.timeNow.hour, this.props.timeNow.minute) + this.props.timeNow.seconds;
        const diff = eventInSecound - nowInSeconds;
        const diffText = diff > 0 ? secondsToHourMinuteSecond(diff) : "Tomorrow";

        return (
            <div className="countdown">
                <strong>{this.props.name}</strong> - {diffText}
                <div className="countdown__icons">
                    <i className="icon edit" onClick={() => {this.props.onEditInit(this.props.id)}} />
                    <i className="icon times" onClick={() => this.props.onRemove(this.props.id)} />
                </div>
                <Overlay>
                    <h3>{this.props.name}</h3>
                    <p>{(this.props.hour).toString().padStart(2, 0)}:{(this.props.minute).toString().padStart(2, 0)}</p>
                </Overlay>
            </div>
        )
    }
};

Countdown.propTypes = {
    name: PropTypes.string,
    hour: PropTypes.number,
    minute: PropTypes.number,
    timeNow: PropTypes.shape({
        hour: PropTypes.number,
        minute: PropTypes.number,
        seconds: PropTypes.number
    }),
    onEditInit: PropTypes.func,
    onRemove: PropTypes.func
}

export default Countdown;