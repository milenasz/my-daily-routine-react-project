import React, { Component } from "react";
import PropTypes from 'prop-types';
import './EditEvent.css';
import './../../node_modules/semantic-ui-css/semantic.css';

import { isValidNumberInput, parseInputAsNumber, isValidHour, isValidMinute, isValidName } from '../utils';

class EditEvent extends Component {

    constructor(props) {
        super(props);
        this.props = props;
    }

    render() {
        const addBtnClass = this.props.visible === true ? "edit-event__add-new-btn--invisible" : "edit-event__add-new-btn";
        const editEventFormClass = this.props.visible === true ? "edit-event__form edit-event__form--visible" : "edit-event__form";
        const isFormValid = isValidHour(this.props.hour) && isValidMinute(this.props.minute) && isValidName(this.props.name);

        return (
            <div className="edit-event">
                <div className={addBtnClass}>
                    <i className="icon plus" onClick={() => this.props.toggleVisible()} />
                </div>
                <div className={editEventFormClass}>
                    <div className="edit-event__input-group">
                        <label htmlFor="name">name</label>
                        <input
                            type="text"
                            id="name"
                            name="name"
                            value={this.props.name}
                            onChange={e =>
                                this.props.onInputChange({ [e.target.name]: e.target.value })
                            }
                        />
                    </div>
                    <div className="edit-event__input-group">
                        <label htmlFor="hour">hour</label>
                        <input
                            type="tel"
                            id="hour"
                            name="hour"
                            value={this.props.hour === -1 ? "" : this.props.hour}
                            onKeyPress={e => isValidNumberInput(e)}
                            onChange={e =>
                                this.props.onInputChange({ [e.target.name]: parseInputAsNumber(e.target.value) })
                            }
                        />
                    </div>
                    <div className="edit-event__input-group">
                        <label htmlFor="minute">minute</label>
                        <input
                            type="tel"
                            id="minute"
                            name="minute"
                            value={this.props.minute === -1 ? "" : this.props.minute}
                            onKeyPress={e => isValidNumberInput(e)}
                            onChange={e =>
                                this.props.onInputChange({ [e.target.name]: parseInputAsNumber(e.target.value) })
                            }
                        />
                    </div>
                    <button disabled={!isFormValid} onClick={() => this.props.onSave()}>OK</button>
                    <button
                        onClick={() => {
                            this.props.onCancel();
                            this.props.toggleVisible();
                        }}
                        >Cancel
                    </button>
                </div>
            </div>
        );
    }
}

EditEvent.propTypes = {
    name: PropTypes.string,
    hour: PropTypes.number,
    minute: PropTypes.number,
    onInputChange: PropTypes.func,
    onSave: PropTypes.func,
    onCancel: PropTypes.func,
    visible: PropTypes.bool,
    toggleVisible: PropTypes.func
}

export default EditEvent;