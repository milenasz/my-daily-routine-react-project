import { secondsToHourMinuteSecond } from './utils';

test('3600 is 01:00:00', () => {
    expect(secondsToHourMinuteSecond(3600)).toBe("01:00:00");
})

test('0 is 00:00:00', () => {
    expect(secondsToHourMinuteSecond(0)).toBe("00:00:00");
})

test('3601 is 01:00:01', () => {
    expect(secondsToHourMinuteSecond(3601)).toBe("01:00:01");
})

test('7000 is 01:56:40', () => {
    expect(secondsToHourMinuteSecond(7000)).toBe("01:56:40");
})

test('19876 is 05:31:16', () => {
    expect(secondsToHourMinuteSecond(19876)).toBe("05:31:16");
})
