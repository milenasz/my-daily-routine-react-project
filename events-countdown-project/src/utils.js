export function isValidNumberInput(e) {
    return isNaN(parseInt(e.key, 10)) ? e.preventDefault() : true;
}

export function parseInputAsNumber(val) {
    return val === "" ? -1 : parseInt(val, 10);
}

export function isValidName(val) {
    return val.length > 0 ? true : false
}

export function isValidHour(val) {
    return (val >= 0 && val <= 24) ? true : false
}

export function isValidMinute(val) {
    return (val >= 0 && val < 60) ? true : false
}

export function hourMinuteToSeconds(hour, minute) {
    return hour * 3600 + minute * 60
}

export function secondsToHourMinuteSecond(second) {
    let seconds = second;
    const hour = Math.floor(seconds / 3600).toString().padStart(2,0);
    seconds -= hour * 3600;
    const minute = Math.floor(seconds / 60).toString().padStart(2,0);
    seconds -= minute * 60;
    seconds = seconds.toString().padStart(2,0);

    return `${hour}:${minute}:${seconds}`
}